import { BrowserRouter, Route, Routes, NavLink } from "react-router-dom";

import Nav from "./Nav";
import React from "react";
import AttendeesList from "./AttendeesList";
import LocationForm from "./LocationForm";
import ConferenceForm from "./ConferenceForm";
import PresentationForm from "./PresentationForm";
import AttendConference from "./AttendConference";
import MainPage from "./MainPage";

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
        <Route index element={<MainPage />} />
          <Route path="conferences">
            <Route path="new" element={<ConferenceForm />} />
          </Route>
          <Route path="attendees">
            <Route path="new" element={<AttendConference />} />
          </Route>
          <Route path="presentation">
            <Route path="new" element={<PresentationForm />} />
          </Route>
          <Route path="locations">
            <Route path="new" element={<LocationForm />} />
          </Route>
          <Route
            path="attendees"
            element={<AttendeesList attendees={props.attendees} />}
          />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
