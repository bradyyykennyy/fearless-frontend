//============================================================================//

import React, { useEffect, useState } from "react";

//============================================================================//

const StateURL = "http://localhost:8000/api/states/";
const LocationURL = "http://localhost:8000/api/locations/";

//============================================================================//

async function PostData(Data) {
  const fetchConfig = {
    method: "post",
    body: JSON.stringify(Data),
    headers: {
      "Content-Type": "application/json",
    },
  };

  const response = await fetch(LocationURL, fetchConfig);

  if (response.ok) {
    const newLocation = await response.json();
    console.log(newLocation);
  }
}

async function FetchData(setStates) {
  const response = await fetch(StateURL);

  if (response.ok) {
    const data = await response.json();
    setStates(data.states);
  }
};

//============================================================================//

function LocationForm() {
  const [states, setStates] = useState([]);
  const [name, setName] = useState("");
  const [state, setState] = useState("");
  const [city, setCity] = useState("");
  const [roomCount, setRoomCount] = useState("");

  //==================================================//

  const HandleNameChange = (event) => {
    const Value = event.target.value;
    setName(Value);
  };

  const HandleRoomCountChange = (event) => {
    const Value = event.target.value;
    setRoomCount(Value);
  };

  const HandleCityChange = (event) => {
    const Value = event.target.value;
    setCity(Value);
  };

  const HandleStateChange = (event) => {
    const Value = event.target.value;
    setState(Value);
  };

  //==================================================//

  const HandleSubmit = (event) => {
    event.preventDefault();

    const data = {}; // Empty JSON

    data.room_count = roomCount;
    data.name = name;
    data.city = city;
    data.state = state;

    console.log(data);

    PostData(data);

    setName("");
    setRoomCount("");
    setCity("");
    setState("");
  };

  const HTMLData = () => {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new location</h1>
            <form onSubmit={HandleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <input
                  onChange={HandleNameChange}
                  placeholder="Name"
                  required
                  type="text"
                  name="name"
                  id="name"
                  className="form-control"
                  value={name}
                />

                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={HandleRoomCountChange}
                  placeholder="Room count"
                  required
                  type="number"
                  id="room_count"
                  name="room_count"
                  className="form-control"
                  value={roomCount}
                />
                <label htmlFor="room_count">Room count</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={HandleCityChange}
                  placeholder="City"
                  required
                  type="text"
                  id="city"
                  name="city"
                  className="form-control"
                  value={city}
                />
                <label htmlFor="city">City</label>
              </div>
              <div className="mb-3">
                <select
                  onChange={HandleStateChange}
                  id="stateselection"
                  required
                  className="form-select"
                  name="state"
                  value={state}
                >
                  <option value="">Choose a state</option>
                  {states.map((state) => {
                    return (
                      <option
                        key={state.abbreviation}
                        value={state.abbreviation}
                      >
                        {state.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  };

  //==================================================//

  useEffect(() => {FetchData(setStates); }, []);

  //==================================================//

  return HTMLData();

  //==================================================//
}

//============================================================================//

export default LocationForm;

//============================================================================//
