//============================================================================//

import React, { useEffect, useState } from "react";

//============================================================================//

const StateURL = "http://localhost:8000/api/states/";
const LocationURL = "http://localhost:8000/api/locations/";
const ConferenceURL = "http://localhost:8000/api/conferences/";

//============================================================================//

async function PostData(Data, ConferenceHREF) {
  let PresentationURL = `http://localhost:8000${ConferenceHREF}presentations/`;

  const fetchConfig = {
    method: "post",
    body: JSON.stringify(Data),
    headers: {
      "Content-Type": "application/json",
    },
  };

  const response = await fetch(PresentationURL, fetchConfig);

  if (response.ok) {
    const newLocation = await response.json();
    console.log(newLocation);
  }
}

async function FetchData(setConferences) {
  const response = await fetch(ConferenceURL);

  if (response.ok) {
    const data = await response.json();
    setConferences(data.conferences);
  }
}

//============================================================================//

function ConferenceForm() {
  //==================================================//

  const [conferences, setConferences] = useState([]);

  const [presenterName, setPresenterName] = useState("");
  const [email, setEmail] = useState("");
  const [companyName, setCompanyName] = useState("");
  const [title, setTitle] = useState("");
  const [synopsis, setSynopsis] = useState("");
  const [conference, setConference] = useState("");

  //==================================================//

  const HandlePresenterNameChange = (event) => {
    const Value = event.target.value;
    setPresenterName(Value);
  };

  const HandleEmailChange = (event) => {
    const Value = event.target.value;
    setEmail(Value);
  };

  const HandleCompanyNameChange = (event) => {
    const Value = event.target.value;
    setCompanyName(Value);
  };

  const HandleTitleChange = (event) => {
    const Value = event.target.value;
    setTitle(Value);
  };

  const HandleSynopsisChange = (event) => {
    const Value = event.target.value;
    setSynopsis(Value);
  };

  const HandleConferenceChange = (event) => {
    const Value = event.target.value;
    setConference(Value);
  };

  //==================================================//

  const HandleSubmit = (event) => {
    event.preventDefault();

    const data = {
      presenter_name: presenterName,
      company_name: companyName,
      presenter_email: email,
      title: title,
      synopsis: synopsis,
    };

    console.log(data);

    PostData(data, conference);

    setPresenterName("");
    setEmail("");
    setCompanyName("");
    setTitle("");
    setSynopsis("");
    setConference("");
  };

  const HTMLData = () => {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={HandleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input
                  onChange={HandlePresenterNameChange}
                  placeholder="Presenter Name"
                  required
                  type="text"
                  id="presenter_name"
                  name="presenter_name"
                  className="form-control"
                  value={presenterName}
                />
                <label htmlFor="presenter_name">Presenter Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={HandleEmailChange}
                  placeholder="Presenter Email"
                  required
                  type="email"
                  id="presenter_email"
                  name="presenter_email"
                  className="form-control"
                  value={email}
                />
                <label htmlFor="presenter_email">Presenter Email</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={HandleCompanyNameChange}
                  placeholder="Company Name"
                  type="text"
                  id="company_name"
                  name="company_name"
                  className="form-control"
                  value={companyName}
                />
                <label htmlFor="company_name">Company Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={HandleTitleChange}
                  placeholder="Title"
                  required
                  type="text"
                  id="title"
                  name="title"
                  className="form-control"
                  value={title}
                />
                <label htmlFor="title">Title</label>
              </div>
              <div className="textarea mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea
                  onChange={HandleSynopsisChange}
                  required
                  className="form-control"
                  name="synopsis"
                  id="synopsis"
                  rows="3"
                  value={synopsis}
                ></textarea>
              </div>

              <div className="mb-3">
                <select
                  onChange={HandleConferenceChange}
                  id="conference-selection"
                  required
                  className="form-select"
                  name="conference"
                  value={conference}
                >
                  <option value="">Choose a conference</option>
                  {conferences.map((conf) => {
                    return (
                      <option key={conf.href} value={conf.href}>
                        {conf.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  };

  //==================================================//

  useEffect(() => {
    FetchData(setConferences);
  }, []);

  //==================================================//

  return HTMLData();

  //==================================================//
}

//============================================================================//

export default ConferenceForm;

//============================================================================//
