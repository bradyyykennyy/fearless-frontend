//============================================================================//

import React, { useEffect, useState } from "react";

//============================================================================//

const StateURL = "http://localhost:8000/api/states/";
const LocationURL = "http://localhost:8000/api/locations/";
const ConferenceURL = 'http://localhost:8000/api/conferences/';

//============================================================================//

async function PostData(Data) {
  const fetchConfig = {
    method: "post",
    body: JSON.stringify(Data),
    headers: {
      "Content-Type": "application/json",
    },
  };

  const response = await fetch(ConferenceURL, fetchConfig);

  if (response.ok) {
    const newLocation = await response.json();
    console.log(newLocation);
  }
}

async function FetchData(setLocations) {
  const response = await fetch(LocationURL);

  if (response.ok) {
    const data = await response.json();
    setLocations(data.locations);
  }
}

//============================================================================//

function ConferenceForm() {
  //==================================================//

  const [locations, setLocations] = useState([]);

  const [location, setLocation] = useState("");
  const [name, setName] = useState("");
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");
  const [description, setDescription] = useState("");
  const [maximumPresentation, setMaximumPresentation] = useState("");
  const [maximumAttendees, setMaximumAttendees] = useState("");

  //==================================================//

  const HandleNameChange = (event) => {
    const Value = event.target.value;
    setName(Value);
  };

  const HandleLocationChange = (event) => {
    const Value = event.target.value;
    setLocation(Value);
  };

  const HandleStartDateChange = (event) => {
    const Value = event.target.value;
    setStartDate(Value);
  };

  const HandleEndDateChange = (event) => {
    const Value = event.target.value;
    setEndDate(Value);
  };

  const HandleDescriptionChange = (event) => {
    const Value = event.target.value;
    setDescription(Value);
  };

  const HandleMaximumPresentationChange = (event) => {
    const Value = event.target.value;
    setMaximumPresentation(Value);
  };

  const HandleMaximumAttendeesChange = (event) => {
    const Value = event.target.value;
    setMaximumAttendees(Value);
  };

  //==================================================//

  const HandleSubmit = (event) => {
    event.preventDefault();

    const data = {
        "name": name,
        "starts": startDate,
        "ends": endDate,
        "description": description,
        "max_presentations": parseInt(maximumPresentation),
        "max_attendees": parseInt(maximumAttendees),
        "location": parseInt(location)
      }

    console.log(data);

    PostData(data);

    setName("");
    setLocation("");
    setStartDate("");
    setEndDate("");
    setDescription("");
    setMaximumPresentation("");
    setMaximumAttendees("");
  };

  const HTMLData = () => {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={HandleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input
                  value={name}
                  onChange={HandleNameChange}
                  placeholder="Name"
                  required
                  type="text"
                  id="name"
                  name="name"
                  className="form-control"
                />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  value={startDate}
                  onChange={HandleStartDateChange}
                  placeholder="Start Data"
                  required
                  type="date"
                  id="start_date"
                  name="starts"
                  className="form-control"
                />
                <label htmlFor="start_date">Start Date</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  value={endDate}
                  onChange={HandleEndDateChange}
                  placeholder="End Data"
                  required
                  type="date"
                  id="end_date"
                  name="ends"
                  className="form-control"
                />
                <label htmlFor="start_date">End Date</label>
              </div>
              <div className="textarea mb-3">
                <label htmlFor="description">Description</label>
                <textarea
                  value={description}
                  onChange={HandleDescriptionChange}
                  required
                  className="form-control"
                  name="description"
                  id="description"
                  rows="3"
                ></textarea>
              </div>
              <div className="form-floating mb-3">
                <input
                  value={maximumPresentation}
                  onChange={HandleMaximumPresentationChange}
                  placeholder="Maximum Presentations"
                  required
                  type="number"
                  id="max_presentations"
                  name="max_presentations"
                  className="form-control"
                />
                <label htmlFor="max_presentations">Maximum Presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  value={maximumAttendees}
                  onChange={HandleMaximumAttendeesChange}
                  placeholder="Maximum Attendees"
                  required
                  type="number"
                  id="max_attendees"
                  name="max_attendees"
                  className="form-control"
                />
                <label htmlFor="max_attendees">Maximum Attendees</label>
              </div>
              <div className="mb-3">
                <select
                  value={location}
                  onChange={HandleLocationChange}
                  id="location-selection"
                  required
                  className="form-select"
                  name="location"
                >
                  <option value="">Choose a location</option>
                  {locations.map((locationio) => {
                    return (
                      <option
                        key={locationio.id}
                        value={locationio.id}
                      >
                        {locationio.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  };

  //==================================================//

  useEffect(() => {
    FetchData(setLocations);
  }, []);

  //==================================================//

  return HTMLData();

  //==================================================//
}

//============================================================================//

export default ConferenceForm;

//============================================================================//
