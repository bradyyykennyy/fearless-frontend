//============================================================================//

import React, { useEffect, useState } from "react";

//============================================================================//

const StateURL = "http://localhost:8000/api/states/";
const LocationURL = "http://localhost:8000/api/locations/";
const ConferenceURL = "http://localhost:8000/api/conferences/";

//============================================================================//

function wait(sec) {
  const date = Date.now();
  let currentDate = null;
  do {
    currentDate = Date.now();
  } while (currentDate - date < sec * 1000);
}

async function PostData(Data, ConferenceHREF) {
  let AttendeesURL = `http://localhost:8001${ConferenceHREF}attendees/`;
  const fetchConfig = {
    method: "post",
    body: JSON.stringify(Data),
    headers: {
      "Content-Type": "application/json",
    },
  };

  const response = await fetch(AttendeesURL, fetchConfig);

  return response.ok ? true : false

}

async function FetchData(setConferences) {
  const response = await fetch(ConferenceURL);

  if (response.ok) {
    let data = await response.json();

    let conferenceSelection = document.querySelector("#conference-selection");
    let conferenceSpinner = document.querySelector(
      "#loading-conference-spinner"
    );

    setConferences(data.conferences);

    conferenceSpinner.className += " d-none";

    conferenceSelection.classList.remove("d-none");
  }
}

//============================================================================//

function ConferenceForm() {
  //==================================================//

  const [conferences, setConferences] = useState([]);
  const [hasSignedUp, setHasSignedUp] = useState(false);

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [conference, setConference] = useState("");

  //==================================================//

  const formClasses = !hasSignedUp ? "" : "d-none";
  const messageClasses = !hasSignedUp
    ? "alert alert-success d-none mb-0"
    : "alert alert-success mb-0";

  //==================================================//

  const HandleEmailChange = (event) => {
    const Value = event.target.value;
    setEmail(Value);
  };

  const HandleNameChange = (event) => {
    const Value = event.target.value;
    setName(Value);
  };

  const HandleConferenceChange = (event) => {
    const Value = event.target.value;
    setConference(Value);
  };

  //==================================================//

  const HandleSubmit = async (event) => {
    event.preventDefault();

    const data = {
      name: name,
      email: email,
    };

    let Status = await PostData(data, conference);

    if (Status == true) {
      setHasSignedUp(true);
      setName("");
      setEmail("");
      setConference("");
    }
  };

  const HTMLData = () => {
    return (
      <div className="my-5">
        <div className="row">
          <div className="col col-sm-auto">
            <img
              width="300"
              className="bg-white rounded shadow d-block mx-auto mb-4"
              src="logo.svg"
            />
          </div>
          <div className="col">
            <div className="card shadow">
              <div className="card-body">
                <form
                  className={formClasses}
                  onSubmit={HandleSubmit}
                  id="create-attendee-form"
                >
                  <h1 className="card-title">It's Conferencing Time!</h1>
                  <p className="mb-3">
                    Please choose which conference you'd like to attend.
                  </p>
                  <div
                    className="d-flex justify-content-center mb-3"
                    id="loading-conference-spinner"
                  >
                    <div className="spinner-grow text-secondary" role="status">
                      <span className="visually-hidden">Loading...</span>
                    </div>
                  </div>
                  <div className="mb-3">
                    <select
                      onChange={HandleConferenceChange}
                      name="conference"
                      id="conference-selection"
                      className="form-select d-none"
                      required
                    >
                      <option value="">Choose a conference</option>
                      {conferences.map((conf) => {
                        return (
                          <option key={conf.href} value={conf.href}>
                            {conf.name}
                          </option>
                        );
                      })}
                    </select>
                  </div>
                  <p className="mb-3">Now, tell us about yourself.</p>
                  <div className="row">
                    <div className="col">
                      <div className="form-floating mb-3">
                        <input
                          onChange={HandleNameChange}
                          value={name}
                          required
                          placeholder="Your full name"
                          type="text"
                          id="name"
                          name="name"
                          className="form-control"
                        />
                        <label htmlFor="name">Your full name</label>
                      </div>
                    </div>
                    <div className="col">
                      <div className="form-floating mb-3">
                        <input
                          onChange={HandleEmailChange}
                          value={email}
                          required
                          placeholder="Your email address"
                          type="email"
                          id="email"
                          name="email"
                          className="form-control"
                        />
                        <label htmlFor="email">Your email address</label>
                      </div>
                    </div>
                  </div>
                  <button className="btn btn-lg btn-primary">I'm going!</button>
                </form>
                <div className={messageClasses} id="success-message">
                  Congratulations! You're all signed up!
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };

  //==================================================//

  useEffect(() => {
    FetchData(setConferences);
  }, []);

  //==================================================//

  return HTMLData();

  //==================================================//
}

//============================================================================//

export default ConferenceForm;

//============================================================================//
